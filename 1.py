# coding=utf-8
import requests,json

jq_base_url="https://dataapi.joinquant.com/apis"

#获取调用凭证
body={
    "method": "get_token",
    "mob": "17621445015",  #mob是申请JQData时所填写的手机号
    "pwd": "13277368872",  #Password为聚宽官网登录密码，新申请用户默认为手机号后6位
}

response = requests.post(jq_base_url, data = json.dumps(body))

print response.text

token=response.text

#调用get_security_info获取单个标的信息
body={
    "method": "get_security_info",
    "token": token,
    "code": "603501.XSHG",
}

response = requests.post(jq_base_url, data = json.dumps(body))

print response.text


# 01获取标的基本信息
# 上海证券交易所	.XSHG
# 深圳证券交易所	.XSHE

# 02获取所有标的信息 get_all_securities(types=[], date=None)
# types: list: 用来过滤securities的类型, list元素可选: 'stock'。types为空时返回所有股票, 不包括基金,指数和期货
# date: 日期, 一个字符串或者 [datetime.datetime]/[datetime.date] 对象, 用于获取某日期还在上市的股票信息. 默认值为 None, 表示获取所有日期的股票信息

# 03 获取单个标的信息get_security_info get_security_info('300738.XSHE',date=None)
# display_name: 中文名称
# name: 缩写简称
# start_date: 上市日期, [datetime.date] 类型
# end_date: 退市日期， [datetime.date] 类型, 如果没有退市则为2200-01-01
# type: 类型，stock(股票)，index(指数)，etf(ETF基金)，fja（分级A），fjb（分级B）
# parent: 分级基金的母基金代码

# 04 获取指数成份股 get_index_stocks('000300.XSHG')
# index_symbol: 指数代码
# date: 查询日期, 一个字符串(格式类似'2015-10-15')或者[datetime.date]/[datetime.datetime]对象, 可以是None,
# 获取一个指数给定日期在平台可交易的成分股列表

# 05 股票代码格式转化  normalize_code(code)
# 将其他形式的股票代码转换为jqdatasdk函数可用的股票代码形式。

# 06 获取概念列表 get_concepts()
# index: 概念代码
# name: 概念名称
# start_date: 开始日期

# 07 获取概念成份股 get_concept_stocks(concept_code, date=None)
# concept_code: 概念板块编码
# date: 查询日期, 一个字符串(格式类似'2015-10-15')或者[datetime.date]/[datetime.datetime]对象, 可以是None.
# 返回 返回股票代码的list

#08 获取指定范围交易日 get_trade_days(start_date=None, end_date=None, count=None)
# 参数
# start_date: 开始日期, 与 count 二选一, 不可同时使用. str/[datetime.date]/[datetime.datetime] 对象
# end_date: 结束日期, str/[datetime.date]/[datetime.datetime] 对象, 默认为 datetime.date.today()
# count: 数量, 与 start_date 二选一, 不可同时使用, 必须大于 0. 表示取 end_date 往前的 count 个交易日，包含 end_date 当天。

#09 获取行情数据 get_price(security, start_date=None, end_date=None, frequency='daily', fields=None, skip_paused=False, fq='pre', count=None, panel=True, fill_paused=True)
# 获取一支或者多只股票、期货、指数场内基金、50etf期权的实时行情和历史行情, 按天或者按分钟；
# frequency为非一天或者一分钟，请使用get_bars;
# 取多支标的的数据时，不要获取交易时段不同的标的（例如：不同交易时间的期货标的），否则会报错；
# 这里在使用时注意 end_date 的设置，不要引入未来的数据；
# 标识时间为09:32:00的1分钟k线，其数据时间为09:31:00至09:31:59；
# 交易所没有提供分钟级别数据，分钟数据需要再次处理，获取实时行情数据时，为了可以保证获取当前分钟的数据，请在第10秒后获取分钟数据
# 参数
# security: 一支股票代码或者一个股票代码的list
# count: 与 start_date 二选一，不可同时使用. 数量, 返回的结果集的行数, 即表示获取 end_date 之前几个 frequency 的数据
# start_date: 与 count 二选一，不可同时使用. 字符串或者 [datetime.datetime]/[datetime.date] 对象, 开始时间.
# 如果 count 和 start_date 参数都没有, 则 start_date 生效, 值是 '2015-01-01'. 注意:
# 当取分钟数据时, 时间可以精确到分钟, 比如: 传入 datetime.datetime(2015, 1, 1, 10, 0, 0) 或者 '2015-01-01 10:00:00'.
# 当取分钟数据时, 如果只传入日期, 则日内时间是当日的 00:00:00.
# 当取天数据时, 传入的日内时间会被忽略
# end_date: 格式同上, 结束时间, 默认是'2015-12-31', 包含此日期. 注意: 当取分钟数据时, 如果 end_date 只有日期, 则日内时间等同于 00:00:00, 所以返回的数据是不包括 end_date 这一天的.
# frequency: 单位时间长度, 几天或者几分钟, 现在支持'Xd','Xm', 'daily'(等同于'1d'), 'minute'(等同于'1m'), X是一个正整数, 分别表示X天和X分钟(不论是按天还是按分钟回测都能拿到这两种单位的数据), 注意, 当X > 1时, fields只支持['open', 'close', 'high', 'low', 'volume', 'money']这几个标准字段. 默认值是daily
# fields: 字符串list, 选择要获取的行情数据字段, 默认是None(表示['open', 'close', 'high', 'low', 'volume', 'money']这几个标准字段), 支持SecurityUnitData里面的所有基本属性,，包含：['open', 'close', 'low', 'high', 'volume', 'money', 'factor', 'high_limit','low_limit', 'avg', 'pre_close', 'paused', 'open_interest'], open_interest为期货持仓量
# skip_paused: 是否跳过不交易日期(包括停牌, 未上市或者退市后的日期). 如果不跳过, 停牌时会使用停牌前的数据填充(具体请看SecurityUnitData的paused属性), 上市前或者退市后数据都为 nan, 但要注意:
# 默认为 False
# 当 skip_paused 是 True 时, 只能取一只股票的信息 关于停牌: 因为此API可以获取多只股票的数据, 可能有的股票停牌有的没有, 为了保持时间轴的一致,我们默认没有跳过停牌的日期, 停牌时使用停牌前的数据填充(请看 [SecurityUnitData] 的 paused 属性). 如想跳过, 请使用 skip_paused=True 参数, 同时只取一只股票的信息
# fq: 复权选项:
# 'pre': 前复权
# None: 不复权, 返回实际价格
# 'post': 后复权
# panel：当传入股票列表时，指定返回结果是否使用panel格式，默认为True；指定panel=False时返回dataframe格式。
# fill_paused：对于停牌股票的价格处理，默认为True；True表示用pre_close价格填充；False 表示使用NAN填充停牌的股票价格。

# pandas的基础操作
# （1）读取 CSV 格式的数据集
# pd.DataFrame.from_csv(“csv_file”)
# pd.read_csv(“csv_file”)
# （2）读取 Excel 数据集
# pd.read_excel("excel_file")
# （3）将 DataFrame 直接写入 CSV 文件
# 如下采用逗号作为分隔符，且不带索引：
# df.to_csv("data.csv", sep=",", index=False)
# （4）基本的数据集特征信息
# df.info()
# （5）基本的数据集统计信息
# print(df.describe())
# (6) Print data frame in a table
# 将 DataFrame 输出到一张表：
# print(tabulate(print_table, headers=headers))
# 当「print_table」是一个列表，其中列表元素还是新的列表，「headers」为表头字符串组成的列表。
# （7）列出所有列的名字
# df.columns
# 8）删除缺失数据
# df.dropna(axis=0, how='any')
# 返回一个 DataFrame，其中删除了包含任何 NaN 值的给定轴，选择 how=「all」会删除所有元素都是 NaN 的给定轴。
# （9）替换缺失数据
# df.replace(to_replace=None, value=None)
# 使用 value 值代替 DataFrame 中的 to_replace 值，其中 value 和 to_replace 都需要我们赋予不同的值。
# （10）检查空值 NaN
# pd.isnull(object)
# 检查缺失值，即数值数组中的 NaN 和目标数组中的 None/NaN。
# （11）删除特征
# df.drop('feature_variable_name', axis=1)
# axis 选择 0 表示行，选择表示列。
# （12）将目标类型转换为浮点型
# pd.to_numeric(df["feature_name"], errors='coerce')
# 将目标类型转化为数值从而进一步执行计算，在这个案例中为字符串。
# （13）将 DataFrame 转换为 NumPy 数组
# df.as_matrix()
# （14）取 DataFrame 的前面「n」行
# df.head(n)
# （15）通过特征名取数据
# df.loc[feature_name]
# （16）对 DataFrame 使用函数
# 该函数将令 DataFrame 中「height」列的所有值乘上 2：
# df["height"].apply(*lambda* height: 2 * height)
# 或：
# def multiply(x):
#  return x * 2
# df["height"].apply(multiply)
# （17）重命名列
# 下面代码会重命名 DataFrame 的第三列为「size」：
# df.rename(columns = {df.columns[2]:'size'}, inplace=True)
# （18）取某一列的唯一实体
# 下面代码将取「name」列的唯一实体：
# df["name"].unique()
# （19）访问子 DataFrame
# 以下代码将从 DataFrame 中抽取选定了的列「name」和「size」：
# new_df = df[["name", "size"]]
# （20）总结数据信息
# # Sum of values in a data frame
# df.sum()
# # Lowest value of a data frame
# df.min()
# # Highest value
# df.max()
# # Index of the lowest value
# df.idxmin()
# # Index of the highest value
# df.idxmax()
# # Statistical summary of the data frame, with quartiles, median, etc.
# df.describe()
# # Average values
# df.mean()
# # Median values
# df.median()
# # Correlation between columns
# df.corr()
# # To get these values for only one column, just select it like this#
# df["size"].median()
# （21）给数据排序
# df.sort_values(ascending = False)
# （22）布尔型索引
# 以下代码将过滤名为「size」的列，并仅显示值等于 5 的列：
# df[df["size"] == 5]
# （23）选定特定的值
# 以下代码将选定「size」列、第一行的值：
# df.loc([0], ['size'])