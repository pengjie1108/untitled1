# coding=utf-8
import requests,json
import time

####### 固定数据

# 创造一个hearder
def build_head(referlink):

    headers = {'user-agent': 'Mozilla/5.0 (Linux; U; Android 10; zh-cn; MI 8 Build/QKQ1.190828.002) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 MQQBrowser/10.2 Mobile Safari/537.36',
                   'Host':'www.365ying.cn',
                   'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                   'Accept-Language':'zh-CN,en-US;q=0.9',
                   'Accept-Encoding':'gzip, deflate',
                   'Content-Type':'application/x-www-form-urlencoded',
                   'Referer':referlink,
                   'Connection':'keep-alive',
            }

    return headers

# 创造一个cookies
def build_cookies():
    # 打开所保存的cookies内容文件
    f = open(r'cookies', 'r')

    # 初始化cookies字典变量
    cookies = {}

    # 按照字符：进行划分读取
    for line in f.read().split(';'):
        # 其设置为1就会把字符串拆分成2份
        name, value = line.strip().split('=', 1)
        # 为字典cookies添加内容
        if name == "lastlogin_time":
            value = str(int(time.time()))
        cookies[name] = value

    return cookies

# 打印headers
def print_headers(headers):
    for h in headers:
        print(h+" : "+headers[h] + '\r\n')

# 打印cookies
def print_Cookies(cookies):
    for h in cookies:
        print(h+" : "+cookies[h] + '\r\n')


####### 调用方法

# 登录
def login(account,pwd):
    # 登录urAPI
    jq_base_url = "https://www.365ying.cn/API/index/login"
    # 登录请求
    params = {
        "account": account,
        "pwd": pwd,
    }
    # 请求头
    headers = build_head(jq_base_url)

    response = requests.post(url=jq_base_url, params=params, headers=headers)

    return response

# 查看所有持仓
def see_my_freedom_states():
    # url
    gu_list_url = "https://www.365ying.cn/index/strategy/strategy_ajax"

    # header
    headers_user = build_head("https://www.365ying.cn/index/strategy/strategy.html")

    # cookies
    cookies = build_cookies()

    res = requests.request("GET", gu_list_url, headers=headers_user, cookies=cookies)
    # print res.content
    return json.loads(res.content)

# 股票名称搜索
def seach_stockname(name):
    # url
    seach_stock_url = "https://www.365ying.cn/index/strategy/search_stock_api"

    # cookies
    cookies = build_cookies()

    #prams
    params = {"content": name}
    res = requests.post(seach_stock_url, data = params, json=None, cookies=cookies)

    return json.loads(res.content)

# 查询交易杠杆
def search_max_lever():
    # url
    search_max_lever = "https://www.365ying.cn/API/info/bs"

    # header
    headers_user = build_head("https://www.365ying.cn/index/strategy/strategy.html")

    # cookies
    cookies = build_cookies()

    res = requests.request("GET", search_max_lever, headers=headers_user, cookies=cookies)

    # print res.content
    return json.loads(res.content)

# 增加保证金
def increase_credit(countid,money):
    # url
    increase_credit = "https://www.365ying.cn/API/index/updateCreditValue"

    # cookies
    cookies = build_cookies()

    # prams
    params = {"id": countid,
              "trues": "1",
              "credit_value": money
              }

    res = requests.get(increase_credit, params=params, cookies=cookies)
    # print res.content

    return res.content

# 查看盘口
def search_stock_price(code):
    # url
    seach_stock_price_url = "https://www.365ying.cn/index/strategy/getNewData"

    # cookies
    cookies = build_cookies()

    # prams
    params = {"gid": code}
    res = requests.post(seach_stock_price_url, data=params, json=None, cookies=cookies)

    # print res.content
    return json.loads(res.content)

# 查看大盘行情
def see_graindex():
    # url
    see_graindex_url = "https://www.365ying.cn/api/index/grailindex"

    # header
    headers_user = build_head("https://www.365ying.cn/index/strategy/strategy.html")

    # cookies
    cookies = build_cookies()

    res = requests.request("GET", see_graindex_url, headers=headers_user, cookies=cookies)

    return json.loads(res.content)

# 查看个股可买状态
def check_stock(code):

    # url
    check_stock_url = "https://www.365ying.cn/api/index/checkstockstop"

    # cookies
    cookies = build_cookies()

    # prams
    params = {"gid": code}

    res = requests.post(check_stock_url, params=params, cookies=cookies)
    print res.content

    return res.content

# 查询分时
def serch_stock_minite(code):
    # url
    serch_stock_minite_url = "https://www.365ying.cn/index/strategy/Tstock"

    # cookies
    cookies = build_cookies()

    # prams
    params = {"code": code}
    res = requests.post(serch_stock_minite_url, data=params, json=None, cookies=cookies)

    return json.loads(res.content)

# 下单买入
def buy_stock(stockname,money,lever):
    # url
    buy_stock_url = "https://www.365ying.cn/API/info/createStrategy"

    # cookies
    cookies = build_cookies()

    code = seach_stockname(stockname)['list'][0]['stock_code']
    name = seach_stockname(stockname)['list'][0]['stock_name']
    price = float(search_stock_price(code)['list']['stock_info']['result'][0]['data']['sellOnePri'])
    # 计算
    double_value = money * lever
    can_buy_num = int (int (double_value / price) /100) * 100
    user_money = can_buy_num * price
    fee = 0.0025 * double_value
    loss_ratio = (money - user_money * 0.03) / user_money # 【信用金 - （市值 * 3%）】 / 股票数量 /现价
    loss_value = round((1 - round(loss_ratio * 100, 2) / 100) * price, 2)
    up = (float(price * 11) * 100) / 100

    # prams
    params = {
         "account": "17621445015",
         "strategy_type": 1,
         "stock_name": name,
         "stock_code": code,
         "stock_number": can_buy_num,
         "credit": money,
         "market_value": user_money,
         "double_value": double_value,
         "buy_price": price,
         "surplus_value": up,
         "loss_value": loss_value, # 止损价格
         "defer": 1,
         "buy_poundage": fee,
         "packet_id": "",
         "celue_bs": lever
              }

    res = requests.post(buy_stock_url, data=params, json=None, cookies=cookies)

    print params
    return params

    # print res.content
    # return json.loads(res.content)

# 卖出策略
def sell_stock(strategyid):
    # url
    sell_stock_url = "https://www.365ying.cn/index/strategy/sellStrategy"

    # cookies
    cookies = build_cookies()

    # prams
    params = {"id": strategyid}

    res = requests.post(sell_stock_url, data=params, json=None, cookies=cookies)

    print res.content
    return json.loads(res.content)

# 挂单卖出
def sell_special_stock(strategyid,sell_price):
    # url
    sell_special_stock_url = "https://www.365ying.cn/index/strategy/sellGuadan"

    # cookies
    cookies = build_cookies()

    # prams
    params = {
        "id": strategyid,
         "guadan_jiage" : sell_price}

    res = requests.post(sell_special_stock_url, data=params, json=None, cookies=cookies)

    print res.content
    return json.loads(res.content)

# 申请延递
def change_defer(strategyid):
    # url
    change_defer_url = "https://www.365ying.cn/index/strategy/changeDefer"

    # cookies
    cookies = build_cookies()

    # prams
    params = {
        "id": strategyid}

    res = requests.post(change_defer_url, data=params, json=None, cookies=cookies)

    print res.content
    return json.loads(res.content)


# 撤单
def cancel_guadan(guadanid):
    # url
    cancel_guadan_url = "https://www.365ying.cn/index/strategy/cancelGuadan"

    # cookies
    cookies = build_cookies()

    # prams
    params = {
        "id": guadanid}

    res = requests.post(cancel_guadan_url, data=params, json=None, cookies=cookies)

    print res.content
    return json.loads(res.content)




# 调用 ########

# 检查可买状态
# check_stock(seach_stockname("八方股份")['list'][0]['stock_code'])

# buy_stock("赛升药业",1000,5)

# 查询分时
# print serch_stock_minite(seach_stockname("洽洽食品")['list'][0]['stock_code'])

# # 查询大盘
# print see_graindex()["list"]["showapi_res_body"]["indexList"]

# 查询盘口
# price_list = search_stock_price(seach_stockname("洽洽食品")['list'][0]['stock_code'])['list']

# 增加保证金
# increase_credit("5053","29")

# # 查询杠杆
# lever_list =  search_max_lever()['list']
# print lever_list

# # 调用查询
# stock_info = seach_stockname("洽洽食品")['list'][0]
# print stock_info['stock_code'],stock_info['stock_name'],stock_info['nowprice'],stock_info['rate']
#

# 查看持仓
# money_states = see_my_freedom_states()
# money_status = money_states['list']['celue']['arr']
# print "以下是您的账户说明:"# 打印持股状态
# print "您当前的财富净值是：",money_status['all_money'] + money_status['fudong'],"可用金额是：",money_status['can_money'],"浮动盈亏：",money_status['fudong']
# print "您的目前持仓是：",money_status['all_money']

