# coding=utf-8
from jqdatasdk import *
from PriceData import *
import datetime
import MySQLdb

import pandas as pd

# 打开数据库连接
# db = MySQLdb.connect("localhost", "testuser", "test123", "TESTDB", charset='utf8' )
#
# # 使用cursor()方法获取操作游标
# cursor = db.cursor()
#
# # 使用execute方法执行SQL语句
# cursor.execute("SELECT VERSION()")
#
# # 使用 fetchone() 方法获取一条数据
# data = cursor.fetchone()
#
# print "Database version : %s " % data
#
# # 关闭数据库连接
# db.close()


# 登录jqdata
auth("17621445015", "13277368872")

# 判断登录结果，后续都应判断登录成功后才做操作
is_auth = is_auth()

# 获取当前日期
ISOTIMEFORMAT = '%Y-%m-%d'
today_date = datetime.datetime.now().strftime(ISOTIMEFORMAT)

print today_date

# 手动输入代码
# code = "603489"
#
# # 输入单个code进行转换成 security
# security_code = normalize_code(code)

# 获取单个标的的信息 get_security_info(code,date=None)
# gsi = get_security_info(security_code, today_date)
# start_date = get_security_info(security_code).start_date
# print gsi.display_name


# 自定义函数 -- 获取从上市第一天到目前的所有日线数据 dataframe
def get_all_price_daily(security_code, start_date):
    # 获取行情数据
    price_data_frame = get_price(security_code,
                                 start_date=start_date,
                                 end_date=today_date,
                                 frequency='daily',
                                 fields=['open', 'close', 'high', 'low'],
                                 skip_paused=True,
                                 fq='pre',
                                 count=None,
                                 panel=True,
                                 fill_paused=True)

    return price_data_frame

def get_all_stocks():

     stocklist = get_all_securities(['stock'])

     # print stocklist['display_name']
     # print stocklist['name']
     # print stocklist['start_date']
     # print stocklist['end_date']
     # print stocklist['type']

     print stocklist

     return stocklist

get_all_stocks()


# 判断k线是否有跳空
# def is_jump(price_old,price_new):
#     max_price = max(price_old.price_high,price_old.price_low,price_temp.price_high,price_temp.price_low)
#     min_price = min(price_old.price_high,price_old.price_low,price_temp.price_high,price_temp.price_low)



# index 对于行标签，要用于结果帧的索引是可选缺省值np.arrange(n)，如果没有传递索引值。
# columns 对于列标签，可选的默认语法是 - np.arange(n)。 这只有在没有索引传递的情况下才是这样。
# 列选择  df ['one'] （某一列标签）
# 行选择  标签选择  df.loc['b'] （某一行标签）
# 行选择  位置选择  df.iloc[2] (某一行号)
# 行切片  df[2:4] (某几行)

# 处理日线数据
# price_data_frame = get_all_price_daily(security_code, start_date)
# 从第一根开始画线段
# 取第一根K线的最高最低
# print price_data_frame['low']
# print price_data_frame['high']
# print price_data_frame.iloc[0]

# 初始化一批数据
# price_last = PriceData('0', '0', '0', '0', '0')
# current_direct = 0

# 遍历每一条数据
# for index, row in price_data_frame.iterrows():
#
#     price_temp = PriceData(row.name, row['open'], row['close'], row['high'], row['low'])
#
#     is_higher = 1 if row['high'] > price_last.price_high else 2
#     is_lower = 1 if row['low'] > price_last.price_low else 2
#
#     # 01.判断方向
#     # 如果是第一条数据，存下来赋值price_last,否则在比较结束后赋值
#     if row.name == price_data_frame.iloc[0].name:
#         current_direct = 1
#
#         # 记录最新一条k线记录
#         price_last = price_temp
#     #第二条数据以后
#     else:
#         # 如果创新高，且不创新低，判断为上涨
#         if is_higher * is_lower == 1:
#             current_direct = 1
#         # 如果创新高，且创新低，判断为模糊，需要继续判断
#         # 如果不创新高，且创新低，判断为下跌
#         elif is_higher * is_lower == 4:
#             current_direct = 2
#
#         # 记录最新一条k线记录
#         price_last = price_temp
#
#     # 判断是否有跳空
#     is_jump(price_last,price_temp)
#
#     #打印结果
#     current_direct_name = "上涨" if current_direct == 1 else "下跌"
#     print row.name.strftime(ISOTIMEFORMAT),current_direct_name


# 查询当日剩余可调用条数  get_query_count()
if (is_auth): print(get_query_count())
